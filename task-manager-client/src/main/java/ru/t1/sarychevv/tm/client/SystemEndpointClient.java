package ru.t1.sarychevv.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.endpoint.ISystemEndpoint;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.sarychevv.tm.dto.response.system.ServerAboutResponse;
import ru.t1.sarychevv.tm.dto.response.system.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @Override
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @Override
    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}
