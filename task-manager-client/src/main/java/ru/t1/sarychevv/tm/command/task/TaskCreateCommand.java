package ru.t1.sarychevv.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.task.TaskCreateRequest;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(name, description);
        getTaskEndpoint().createTask(request);
    }

}
