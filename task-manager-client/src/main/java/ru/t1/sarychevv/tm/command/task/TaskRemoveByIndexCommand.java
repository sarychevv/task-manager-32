package ru.t1.sarychevv.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(index);
        getTaskEndpoint().removeTaskByIndex(request);
    }

}
