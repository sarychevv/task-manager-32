package ru.t1.sarychevv.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.data.DataBinarySaveRequest;
import ru.t1.sarychevv.tm.enumerated.Role;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data to binary file";

    @NotNull
    public static final String NAME = "data-save-bin";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        serviceLocator.getDomainEndpointClient().saveDataBinary(new DataBinarySaveRequest());
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
