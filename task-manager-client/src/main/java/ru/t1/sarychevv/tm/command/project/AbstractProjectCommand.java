package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sarychevv.tm.command.AbstractCommand;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.field.ProjectEmptyException;
import ru.t1.sarychevv.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpointClient();
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) throw new ProjectEmptyException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
