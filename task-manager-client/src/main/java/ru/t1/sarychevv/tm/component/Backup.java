package ru.t1.sarychevv.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.command.data.DataBackupSaveCommand;
import ru.t1.sarychevv.tm.dto.request.data.DataBackupLoadRequest;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup extends Thread {

    @NotNull
    public static final String FILE_BACKUP = "./task-manager-client/backup.base64";

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void close() {
        executorService.shutdown();
    }

    public void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (bootstrap.getAuthEndpointClient().getSocket() != null) {
            if (!Files.exists(Paths.get(FILE_BACKUP))) return;
            bootstrap.getDomainEndpointClient().loadDataBackup(new DataBackupLoadRequest());
        }
    }

}
