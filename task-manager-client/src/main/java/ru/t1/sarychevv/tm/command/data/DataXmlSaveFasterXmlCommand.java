package ru.t1.sarychevv.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.data.DataXmlSaveFasterXmlRequest;
import ru.t1.sarychevv.tm.enumerated.Role;

public class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file";

    @NotNull
    public static final String NAME = "data-save-xml-faster";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        serviceLocator.getDomainEndpointClient().saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
