package ru.t1.sarychevv.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sarychevv.tm.dto.request.project.*;
import ru.t1.sarychevv.tm.dto.response.project.*;

@NoArgsConstructor
public final class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpoint {

    public ProjectEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @Override
    public @NotNull ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @Override
    public @NotNull ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @Override
    public @NotNull ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @Override
    public @NotNull ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @Override
    public @NotNull ProjectGetByIdResponse getProjectById(@NotNull final ProjectGetByIdRequest request) {
        return call(request, ProjectGetByIdResponse.class);
    }

    @Override
    public @NotNull ProjectGetByIndexResponse getProjectByIndex(@NotNull final ProjectGetByIndexRequest request) {
        return call(request, ProjectGetByIndexResponse.class);
    }

    @Override
    public @NotNull ProjectListResponse listProject(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @Override
    public @NotNull ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @Override
    public @NotNull ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @Override
    public @NotNull ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @Override
    public @NotNull ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}
