package ru.t1.sarychevv.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Update task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-update-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(index, name, description);
        getTaskEndpoint().updateTaskByIndex(request);
    }

}
