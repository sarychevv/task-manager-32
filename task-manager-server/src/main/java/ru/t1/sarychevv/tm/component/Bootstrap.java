package ru.t1.sarychevv.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.endpoint.*;
import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.api.repository.IUserRepository;
import ru.t1.sarychevv.tm.api.service.*;
import ru.t1.sarychevv.tm.dto.request.data.*;
import ru.t1.sarychevv.tm.dto.request.project.*;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.sarychevv.tm.dto.request.task.*;
import ru.t1.sarychevv.tm.dto.request.user.*;
import ru.t1.sarychevv.tm.endpoint.*;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.model.User;
import ru.t1.sarychevv.tm.repository.ProjectRepository;
import ru.t1.sarychevv.tm.repository.TaskRepository;
import ru.t1.sarychevv.tm.repository.UserRepository;
import ru.t1.sarychevv.tm.service.*;
import ru.t1.sarychevv.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ApplicationAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ApplicationVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::loadDataJsonJaxB);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::saveDataJsonJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::loadDataXmlJaxB);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::saveDataXmlJaxB);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::saveDataYamlFasterXml);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::loadDataYamlFasterXml);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changeUserPassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateUserProfile);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectGetByIdRequest.class, projectEndpoint::getProjectById);
        server.registry(ProjectGetByIndexRequest.class, projectEndpoint::getProjectByIndex);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskGetByIdRequest.class, taskEndpoint::getTaskById);
        server.registry(TaskGetByIndexRequest.class, taskEndpoint::getTaskByIndex);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::getTaskByProjectId);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);
    }


    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }


    public void start() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        initDemoData();
        initServer();
    }


    private void initServer() {
        server.start();
    }

    public void stop() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
        server.stop();
    }

    private void initDemoData() {
        @NotNull final User userCustom = userService.create("user", "user", "user@user.ru");
        @NotNull final User userAdmin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userCustom.getId(), "PROJECT1", "Project1 for userCustom");
        projectService.create(userCustom.getId(), "PROJECT2", "Project2 for userCustom");
        projectService.create(userCustom.getId(), "PROJECT3", "Project3 for userCustom");
        projectService.create(userAdmin.getId(), "PROJECT4", "Project4 for userAdmin");
        projectService.create(userAdmin.getId(), "PROJECT5", "Project5 for userAdmin");

        taskService.create(userCustom.getId(), "TASK1", "Task1 for userCustom");
        taskService.create(userCustom.getId(), "TASK2", "Task2 for userCustom");
        taskService.create(userCustom.getId(), "TASK3", "Task3 for userCustom");
        taskService.create(userCustom.getId(), "TASK4", "Task4 for userCustom");
        taskService.create(userAdmin.getId(), "TASK5", "Task5 for userAdmin");
        taskService.create(userAdmin.getId(), "TASK6", "Task6 for userAdmin");
    }

}
