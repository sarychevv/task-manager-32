package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Task;

public interface IProjectTaskService {

    @Nullable Task bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable Task unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

}
