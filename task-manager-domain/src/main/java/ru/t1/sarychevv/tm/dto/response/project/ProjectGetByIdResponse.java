package ru.t1.sarychevv.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class ProjectGetByIdResponse extends AbstractProjectResponse {

    public ProjectGetByIdResponse(@Nullable final Project project) {
        super(project);
    }

}
