package ru.t1.sarychevv.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.project.*;
import ru.t1.sarychevv.tm.dto.response.project.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request);

    @NotNull
    ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request);

    @NotNull
    ProjectListResponse listProject(@NotNull ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
