package ru.t1.sarychevv.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.User;

@NoArgsConstructor
public class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@Nullable final User user) {
        super(user);
    }

}
