package ru.t1.sarychevv.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException() {
        super();
    }

    public AbstractUserException(@NotNull String message) {
        super(message);
    }

    public AbstractUserException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractUserException(@NotNull String message, @NotNull Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
