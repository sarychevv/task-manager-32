package ru.t1.sarychevv.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
