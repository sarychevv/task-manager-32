package ru.t1.sarychevv.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;
import ru.t1.sarychevv.tm.enumerated.TaskSort;

@Getter
@Setter
@NoArgsConstructor
public class TaskListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sort;

    @Nullable
    private String projectId;

    public TaskListByProjectIdRequest(@Nullable final TaskSort sort, @Nullable final String projectId) {
        this.sort = sort;
        this.projectId = projectId;
    }

}
