package ru.t1.sarychevv.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectGetByIdRequest(@Nullable final String id) {
        this.id = id;
    }

}
