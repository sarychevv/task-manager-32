package ru.t1.sarychevv.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.response.AbstractResponse;
import ru.t1.sarychevv.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<Project> projects;

    public ProjectListResponse(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

}
